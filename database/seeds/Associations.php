<?php

use Illuminate\Database\Seeder;

class Associations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ## **************** Example User **************** ##
        // participation in professional associations

        DB::table('associations')->insert([
            'id' => 1,
            'association_name' => 'Eurographics Portuguese Chapter',
            'description' => 'Membro do Grupo Português de Computação Gráfica',
            'abbreviation' => null,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('associations')->insert([
            'id' => 2,
            'association_name' => 'Associação Portuguesa de Reconhecimento de Padrões',
            'description' => 'Secretário da Assembleia Geral da Associação Portuguesa de Reconhecimento de Padrões',
            'abbreviation' => null,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('associations')->insert([
            'id' => 3,
            'association_name' => 'Eurographics - Association for Computer Graphics',
            'description' => ', Membro da European Association for Computer Graphics – Eurographics',
            'abbreviation' => null,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('associations')->insert([
            'id' => 4,
            'association_name' => 'Grupo Português de Computação Gráfica',
            'description' => 'Membro do Grupo Português de Computação Gráfica',
            'abbreviation' => null,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('associations')->insert([
            'id' => 5,
            'association_name' => 'Sociedade Portuguesa de Biomecânica',
            'description' => 'Secretário do Conselho Fiscal da Sociedade Portuguesa de Biomecânica',
            'abbreviation' => null,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('associations')->insert([
            'id' => 6,
            'association_name' => 'Ordem dos Engenheiros',
            'description' => 'Membro da Ordem dos Engenheiros, da especialidade de Engenharia Electrotécnica, com a Cédula Profissional nº 23804, desde janeiro de 1990',
            'abbreviation' => null,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
