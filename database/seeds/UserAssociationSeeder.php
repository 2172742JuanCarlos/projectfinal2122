<?php

use Illuminate\Database\Seeder;

class UserAssociationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_associations')->delete();
        $array = array(
            ## CIIC data 'is_active' => true
            array('user_id' => 6, 'associations_id' => 1, 'active' => 1, 'member', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
            array('user_id' => 44, 'associations_id' => 3, 'active' => 1, 'member', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
            array('user_id' => 43, 'associations_id' => 2, 'active' => 1, 'member', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
            array('user_id' => 45, 'associations_id' => 4, 'active' => 1, 'member', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
            array('user_id' => 46, 'associations_id' => 5, 'active' => 1, 'member', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')),
        );
        DB::table('user_associations')->insert($array);
    }
}
