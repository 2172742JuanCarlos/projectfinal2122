<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>CIIC</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{ asset('css/agency.min.css') }}" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper" id="app">
        <!--  Change for the new replace with: new_sidebar -->
        @include('components.main.sidebar')
        <!--  id="main"  Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <header class="mb-3">
                <!--  Change for the new replace with: new_navbar -->
                @include('components.main.new_navbar')
            </header>
            <!--  id="main-content" class="content" -->
            <div class="container-fluid">
                @include('components.main.content')
            </div>
        </div>
        <!-- Main Footer -->
        @include('components.main.footer')
    </div>
    <!-- /.content -->
    </div>
    @include('components.main.scripts')
</body>

</html>