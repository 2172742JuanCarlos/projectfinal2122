<div id="sidebar" class="active">
    <div class="sidebar-wrapper active">
        <div class="sidebar-header">
            <div class="d-flex justify-content-between">
                <div class="logo">
                    <a href="/home"><img src="./img/logo.png" alt="CIIC Logo"></a>
                </div>
                <div class="toggler">
                    <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
                </div>
            </div>
        </div>
        <div class="sidebar-menu">
            <ul class="menu">
                <li class="sidebar-title">Menu</li>

                <li class="sidebar-item active ">
                    <router-link to="/home" class="nav-link">
                        <i class="bi bi-grid-fill"></i>
                        <span>Home</span>
                    </router-link>
                </li>

                <li class="sidebar-item  ">
                    <a href="form-layout.html" class='sidebar-link'>
                        <i class="bi bi-file-earmark-medical-fill"></i>
                        <span>Profile</span>
                    </a>
                </li>

                <!--<li class="nav-item has-treeview" v-if="{{Auth::user()->isAdmin}} == 1 || {{Auth::user()->roles[0]->role_id}} == 6">-->
                <li class="sidebar-item  has-sub">
                    <a href="#" class='sidebar-link'>
                        <i class="bi bi-stack"></i>
                        <span>Management</span>
                    </a>
                    <ul class="submenu ">
                        <li class="submenu-item " v-if="{{Auth::user()->isAdmin}} == 1">
                            <router-link to="/AboutEdit">About Us Text Edit</router-link>
                        </li>

                        <li class="submenu-item " v-if="{{Auth::user()->isAdmin}} == 1">
                            <router-link to="/missionEdit">Mission Text Edit</router-link>
                        </li>

                        <li class="submenu-item " v-if="{{Auth::user()->isAdmin}} == 1">
                            <router-link to="/news">News</router-link>
                        </li>

                        <li class="submenu-item " v-if="{{Auth::user()->isAdmin}} == 1">
                            <router-link to="/labs">Labs</router-link>
                        </li>

                        <li class="submenu-item " v-if="{{Auth::user()->isAdmin}} == 1">
                            <router-link to="/users">Users</router-link>
                        </li>

                        <li class="submenu-item " v-if="{{Auth::user()->isAdmin}} == 1">
                            <router-link to="/roles">Roles</router-link>
                        </li>

                        <!--
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                            <i class="fas fa-binoculars"></i>
                            <p>Investigadores</p>
                            </a>
                        </li>-->
                        <!--<li class="nav-item" v-if="{{Auth::user()->isAdmin}} == 1">-->
                        <li class="submenu-item ">
                            <router-link to="/scientific">Reunion</router-link>
                        </li>

                        <li class="submenu-item " v-if="{{Auth::user()->isAdmin}} == 1">
                            <router-link to="/partners">Partners</router-link>
                        </li>

                        <li class="submenu-item " v-if="{{Auth::user()->isAdmin}} == 1">
                            <router-link to="/teams">Teams</router-link>
                        </li>

                        <!--<li class="nav-item" v-if="{{Auth::user()->isAdmin}} == 1 || {{Auth::user()->roles[0]->role_id}} == 6">-->
                        <li class="submenu-item ">
                            <router-link to="/awards">Awards</router-link>
                        </li>

                    </ul>
                </li>

                <li class="sidebar-item  has-sub">
                    <a href="#" class='sidebar-link'>
                        <i class="bi bi-collection-fill"></i>
                        <span>Projects</span>
                    </a>
                    <ul class="submenu ">
                        <li class="submenu-item ">
                            <router-link to="/projects">Projects List</router-link>
                        </li>

                        <li class="submenu-item " v-if="{{Auth::user()->isAdmin}} == 1">
                            <router-link to="/associateResearchers">Associate Researchers</router-link>
                        </li>

                        <li class="submenu-item " v-if="{{Auth::user()->isAdmin}} == 1">
                            <router-link to="/associatePartners">Associate Partners </router-link>
                        </li>
                    </ul>
                </li>

                <!--
                    <li class="nav-item has-treeview">
                    <a href="" class="nav-link">
                    <i class="nav-icon fas fa-scroll lime"></i>
                    <p>
                    Patents Administration
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>

                    <ul class="nav nav-treeview">

                    <li class="nav-item">
                        <router-link to="/patents" class="nav-link">
                        <i class="fas fa-highlighter"></i>
                        <p>Patents List</p>
                        </router-link>
                    </li>

                    <li class="nav-item">
                        <router-link to="/home" class="nav-link">
                        <i class="fas fa-user-plus"></i>
                        <p>Associate Users</p>
                        </router-link>
                    </li>

                    </ul>
                </li>
                -->

                <li class="sidebar-item  has-sub">
                    <a href="#" class='sidebar-link'>
                        <i class="bi bi-collection-fill"></i>
                        <span>Ciencia Vitae</span>
                    </a>
                    <ul class="submenu ">
                        <li class="submenu-item ">
                            <router-link to="/cv_outputs">Outputs</router-link>
                        </li>

                        <li class="submenu-item ">
                            <router-link to="/cv_degrees">Degrees</router-link>
                        </li>

                        <li class="submenu-item ">
                            <router-link to="/cv_person_info">Person Info</router-link>
                        </li>

                        <li class="submenu-item ">
                            <router-link to="/cv_language_competency">Language Competency</router-link>
                        </li>

                        <li class="submenu-item ">
                            <router-link to="/cv_mailingadresses">Mailing Addresses</router-link>
                        </li>

                        <li class="submenu-item ">
                            <router-link to="/cv_phones">Phones</router-link>
                        </li>

                        <li class="submenu-item ">
                            <router-link to="/cv_web">Web Addresses</router-link>
                        </li>

                        <li class="submenu-item ">
                            <router-link to="/cv_privileges">Access Privileges</router-link>
                        </li>

                        <li class="submenu-item ">
                            <router-link to="/cv_author">Author Identifier</router-link>
                        </li>

                        <li class="submenu-item ">
                            <router-link to="/cv_citation">Citation Names</router-link>
                        </li>

                        <li class="submenu-item ">
                            <router-link to="/cv_employment">Employment</router-link>
                        </li>

                        <li class="submenu-item ">
                            <router-link to="/cv_groups">Groups</router-link>
                        </li>

                        <li class="submenu-item ">
                            <router-link to="/cv_person_info">Statistics</router-link>
                        </li>
                    </ul>
                </li>

                <li class="sidebar-item  ">
                    <a href="form-layout.html" class='sidebar-link'>
                        <i class="bi bi-file-earmark-medical-fill"></i>
                        <span>Form Layout</span>
                    </a>
                </li>

                <!--<li class="sidebar-item  ">
                            <a href="https://github.com/zuramai/mazer#donate" class='sidebar-link'>
                                <i class="bi bi-logout"></i>
                                <span>Logout</span>
                            </a>
                        </li>-->

            </ul>
        </div>
        <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
    </div>
</div>