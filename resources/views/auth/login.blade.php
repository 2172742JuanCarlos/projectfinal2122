<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CIIC - Computer Science and Communication Research Centre</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>

<body>
    <div id="auth">
        <div class="row h-100">
            <div class="col-lg-6 col-xs-12">
                <div id="auth-left">
                    <div class="auth-logo">
                        <a href="index.html"><img class="w-100 h-100" src="../img/ciiclogo.png" alt="CIIC"></a>
                    </div>
                    <h1 class="auth-title">{{ __('Login') }}</h1>
                    <hr class="mb-4">

                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <span>
                            <input type="hidden" class="{{ $errors->has('email') ? ' is-invalid' : '' }}">
                            @if ($errors->has('email'))
                            <span class="invalid-feedback mb-1" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </span>
                        <div class="form-group position-relative has-icon-left mb-4">
                            <input id="email" type="text" name="email"
                                class="form-control form-control-xl {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('E-Mail Address') }}" value="{{ old('email') }}" required autofocus>
                            <div class="form-control-icon">
                                <i class="bi bi-person"></i>
                            </div>
                        </div>

                        <span>
                            @if ($errors->has('password'))
                            <input type="hidden" class="{{ $errors->has('password') ? ' is-invalid' : '' }}">
                            <span class="invalid-feedback mb-1" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </span>
                        <div class="form-group position-relative has-icon-left mb-4">
                            <input id="password" type="password" name="password" class="form-control form-control-xl"
                                placeholder="{{ __('Password') }}">
                            <div class="form-control-icon {{ $errors->has('password') ? ' is-invalid' : '' }}" required>
                                <i class="bi bi-shield-lock"></i>
                            </div>
                        </div>

                        <div class="form-check form-check-lg d-flex align-items-end">
                            <input class="form-check-input me-2" name="remember" id="remember" type="checkbox" value=""
                                id="flexCheckDefault" {{ old('remember') ? 'checked' : '' }}>
                            <label class="form-check-label text-gray-600" for="flexCheckDefault">
                                {{ __('Remember Me') }}
                            </label>
                        </div>

                        <button type="submit"
                            class="btn btn-primary btn-block btn-lg shadow-lg mt-5">{{ __('Login') }}</button>
                    </form>

                    <div class="text-center mt-5 text-lg fs-4">
                        <p><a class="font-bold" href="auth-forgot-password.html">{{ __('Forgot Your Password?') }}</a></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-xs-12 d-none d-lg-block">
                <div id="auth-right"></div>
            </div>
        </div>

    </div>
</body>

</html>