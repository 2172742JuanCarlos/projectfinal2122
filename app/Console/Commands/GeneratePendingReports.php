<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use App\Http\Controllers\ReportsController;
use Carbon\Carbon;

class GeneratePendingReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:reports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate pending file Reports';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // to run cron, run this comand "php artisan generate:reports"
        echo "Starting job . . .\n";
        $table = 'reports';
        $reports =  DB::table($table)
                    ->where('status', '=', 'P')
                    ->get()->toArray();

        if(!empty($reports)) {
            $reportController = new ReportsController();
            echo "Found: ". count($reports) . " report(s) \r\n";
            echo "Starting cron . . .\r\n";

            foreach ($reports as $report) {
                $start_date = strtotime($report->start_date);
                $end_date = strtotime($report->end_date);
                $file_name = $report->name . '_'. $report->id;
                $result = $reportController->getFileReport(date('Y', $start_date), $start_date, $end_date ,$file_name, true);
                echo ".\r\n";

                if($result['status'] === 'failed') {
                    echo "Error File in file" . $report->name;
                } else {
                    DB::table($table)
                    ->where('id', $report->id)
                    ->update([
                        'name' => $file_name,
                        'path' => $result['path'],
                        'status' => 'C',
                        'updated_at' => Carbon::now()
                    ]);
                    echo "File created:" . $report->name;
                }
               // if sucess change status and add path
            }

            echo ".\r\n";
        } else {
            echo "No Reports found.\r\n";
        }
        echo 'Done..';
    }
}
