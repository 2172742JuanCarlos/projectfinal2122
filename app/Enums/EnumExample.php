<?php
namespace App\Enums;

use Rexlabs\Enum\Enum;

/**
 * The JSON enum.
 *
 * @method static self HEX_TAG()
 * @method static self HEX_AMP()
 * @method static self HEX_APOS()
 * @method static self HEX_QUOT()
 */
class EnumExample extends Enum
{
    const HEX_TAG = 1;
    const HEX_AMP = 2;
    const HEX_APOS = 4;
    const HEX_QUOT = 8;

    /**
     * Retrieve a map of enum keys and values.
     *
     * @return array
     */
    public static function map() : array
    {
        return [
            static::HEX_TAG => 'Hex Tag',
            static::HEX_AMP => 'Hex Amp',
            static::HEX_APOS => 'Hex Apos',
            static::HEX_QUOT => 'Hex Quot',
        ];
    }
}