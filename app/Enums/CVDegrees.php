<?php

namespace App\Enums;

use Rexlabs\Enum\Enum;

/**
 * The CVOutputCategory enum.
 *
 * @see https://api.cienciavitae.pt/schemas/curriculum/common/common-enum.xsd
 */
class CVDegrees extends Enum
{
    const LICENCIATURE = '2';
    const MASTER = '3';
    const DOCTORATE = '5';
}
