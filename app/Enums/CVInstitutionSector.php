<?php

namespace App\Enums;

use Rexlabs\Enum\Enum;

/**
 * The CVInstitutionSector enum.
 *
 * @see https://api.cienciavitae.pt/schemas/curriculum/common/common-enum.xsd
 */
class CVInstitutionSector extends Enum
{
    const Other = 'S00';
    const Academic = 'S01';
    const Companies = 'S02';
    const Health = 'S03';
    const Institutions = 'S04';
    const Government = 'S05';
}
