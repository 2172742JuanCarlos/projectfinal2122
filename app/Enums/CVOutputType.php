<?php

namespace App\Enums;

use Rexlabs\Enum\Enum;

/**
 * The CVOutputType enum.
 *
 * * @see https://api.cienciavitae.pt/schemas/curriculum/common/common-enum.xsd
 */
class CVOutputType extends Enum
{
    //P1
    const JOURNAL_ARTICLE = 'P101';
    const JOURNAL_ISSUE = 'P102';
    const BOOK = 'P103';
    const EDITED_BOOK = 'P104';
    const BOOK_CHAPTER = 'P105';
    const BOOK_REVIEW = 'P106';
    const TRANSLATION = 'P107';
    const THESIS_DISSERTATION = 'P108';
    const NEWSPAPER_ARTICLE = 'P110';
    const NEWSLETTER_ARTICLE = 'P111';
    const ENCYCLOPEDIA_ENTRY = 'P112';
    const MAGAZINE_ARTICLE = 'P113';
    const DICTIONARY_ENTRY = 'P114';
    const REPORT = 'P115';
    const WORKING_PAPER = 'P116';
    const MANUAL = 'P118';
    const ONLINE_RESOURCE = 'P119';
    const TEST = 'P120';
    const WEBSITE = 'P121';
    const CONFERENCE_PAPER = 'P122';
    const CONFERENCE_ABSTRACT = 'P123';
    const CONFERENCE_POSTER = 'P124';
    const EXHIBITION_CATALOG = 'P125';
    const PREFACE_POSTSCRIPT = 'P126';
    // P3
    const ARTISTIC_EXHIBITION = 'P301';
    const AUDIO_RECORDING = 'P302';
    const MUSICAL_COMPOSITION = 'P304';
    const MUSICAL_PERFORMANCE = 'P305';
    const PROGRAM_SHOW = 'P306';
    const SCRIPT = 'P307';
    const SHORT_FICTION = 'P308';
    const THEATRE_PLAY = 'P309';
    const VIDEO_RECORDING = 'P310';
    const VISUAL_ARTWORK = 'P311';
    const SOUND_DESIGN = 'P312';
    const SET_DESIGN = 'P313';
    const LIGHT_DESIGN = 'P314';
    const CHOREOGRAPHY = 'P315';
    const CURATORIAL_MUSEUM_EXHIBITION = 'P316';
    const ARTISTIC_PERFORMANCE = 'P317';
    //P4
    const PATENT = 'P401';
    const LICENSING_PATENT_RIGHTS = 'P402';
    const PROVISIONAL_APPLICATION_FOR_PATENT = 'P403';
    const COPYRIGHT_REGISTRATION = 'P404';
    const TRADEMARK = 'P405';
    //P5
    const STANDARD_OR_POLICY = 'P501';
    const INVENTION = 'P502';
    const DATASET = 'P503';
    const LEGAL_PROCEEDING = 'P504';
    const RESEARCH_TECHNIQUE = 'P505';
    const SPIN_OFF_COMPANY = 'P506';
    const TECHNICAL_STANDARD = 'P507';
    const OTHER_OUTPUT = 'P508';

}
