<?php

namespace App\Enums;

use Rexlabs\Enum\Enum;

/**
 * The CVOutputCategory enum.
 *
 * @see https://api.cienciavitae.pt/schemas/curriculum/common/common-enum.xsd
 */
class CVOutputCategory extends Enum
{
    const PUBLICATION = 'P1';
    const ARTISTIC_INTERPRETATION = 'P3';
    const INTELLECTUAL_PROPERTY = 'P4';
    const OTHERS = 'P5';
}
