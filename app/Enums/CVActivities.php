<?php

namespace App\Enums;

use Rexlabs\Enum\Enum;

/**
 * The CVActivities enum.
 *
 * @see https://api.cienciavitae.pt/schemas/curriculum/common/common-enum.xsd
 */
class CVActivities extends Enum
{
    const CONSULTING = 'S101';
    const LEGAL_PROCEEDING = 'S102';
    const JOURNAL_SCIENTIFIC_COMMITTEE = 'S103';
    const CONFERENCE_SCIENTIFIC_COMMITTEE = 'S104';
    const JURY_ACADEMIC_DEGREE = 'S105';
    const EVALUATION_COMMITTEE = 'S106';
    const MENTORING_TUTORING = 'S107';
    const INTERVIEW_RADIO_SHOW =  'S108';
    const INTERVIEW_NEWSPAPER_MAGAZINE = 'S109';
    const SUPERVISION = 'S110';
    const OTHER_JURY_EVALUATION = 'S111';
    const SCIENTIFIC_EXPEDITION = 'S112';
    const EVENT_ORGANISATION = 'S201';
    const EVENT_PARTICIPATION = 'S202';
    const ASSOCIATION_MEMBER = 'S203';
    const COURSE_DISCIPLINE_TAUGHT = 'S204';
    const ORAL_PRESENTATION = 'S205';
    const COMMITTEE_MEMBER = 'S206';
}
