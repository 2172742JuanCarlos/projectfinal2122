<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberAssociations extends Model
{
    protected $table ="user_associations";

    protected $fillable = [
        'user_id',
        'associations_id',
        'active'
    ];
}
