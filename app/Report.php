<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = [
        'name',
        'path',
        'status',
        'start_date',
        'end_date',
        'created_at',
        'updated_at'
    ];

}