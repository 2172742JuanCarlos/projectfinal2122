<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Association extends Model
{
    protected $table="associations";
    protected $fillable = [
        'association_name',
        'description',
        'abbreviation'
    ];

}
