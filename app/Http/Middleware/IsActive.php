<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\User;

class IsActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check())
        {
            if (auth()->user()->isActive == 0)
            {
                Auth::logout();
                throw new AccessDeniedHttpException("Unauthorized!");
            }
            if(empty(auth()->user()->roles->toArray()) && auth()->user()->isAdmin == 0) {
                $user = User::findOrFail(auth()->user()->id);
                $user->update(['isActive' =>  0]);
                Auth::logout();
                throw new AccessDeniedHttpException("Unauthorized!");
            }
        }
        return $next($request);

        /*if (auth()->user() && auth()->user()->isActive == 1){
            return $next($request);
        }else{
            Auth::logout();
            throw new AccessDeniedHttpException('Unauthorized!');
        }*/
    }
}
