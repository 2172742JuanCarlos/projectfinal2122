<?php

namespace App\Http\Controllers;

use App\Http\Resources\MemberAssocaitonResource;
use App\MemberAssociations;
use Illuminate\Http\Request;

use App\Http\Resources\MemberRolesResource;
use App\MemberRoles;
use App\Http\Controllers\RoleController;
use Illuminate\Support\Facades\DB;
use App\User;
use Carbon\Carbon;
use Svg\Tag\Rect;

class MemberRolesController extends Controller
{
    public function createMemberRole(Request $request){

        $request->validate([
            'userSelected' => 'required',
            'roleSelected' => 'required',
            'functionSelected' => 'required',
        ]);

        $user_selected = $request->userSelected;

        $remove_previous = true;

        if ($remove_previous) {
            $params['select'] = 'user_roles.role_id';
            $roles = RoleController::getUserRole($user_selected, $params);
            $roles = array_column($roles,'role_id');
            $params['fields'] = [
                'active' => '0'
            ];
            self::changeRole($roles, [$user_selected], $params);
        }

        $memberRole = new MemberRoles();

        $memberRole->user_id = $user_selected;
        $memberRole->role_id = $request->functionSelected;
        $memberRole->created_at = Carbon::now();
        $memberRole->updated_at = Carbon::now();

        $memberRole->save();

        return new MemberRolesResource($memberRole);

    }
    public function createMemberAssociation(Request $request){

        $request->validate([
            'userSelected' => 'required',
            'roleSelected' => 'required',
        ]);


        $user_selected = $request->userSelected;

        $remove_previous = true;

        if ($remove_previous) {
            $params['select'] = 'user_associations.associations_id';
            $roles = AssociationController::getUserAssociation($user_selected, $params);
            $roles = array_column($roles,'associations_id');
            $params['fields'] = [
                'active' => '0'
            ];
            self::changeRole($roles, [$user_selected], $params);
        }

        $memberRole = new MemberAssociations();

        $memberRole->user_id = $user_selected;
        $memberRole->associations_id = $request->roleSelected;
        $memberRole->created_at = Carbon::now();
        $memberRole->updated_at = Carbon::now();

        $memberRole->save();

        return new MemberRolesResource($memberRole);

    }

    public function changeRole($role_ids, $user_ids, $params = ''){
        if (empty($user_ids) && !is_array($user_ids) && empty($role_ids) && !is_array($role_ids) ) {
            return false;
        }

        $params['fields'] = $params['fields'] ?? '';

        return DB::table('user_roles')
        ->whereIn('user_id', $user_ids)
        ->whereIn('role_id',  $role_ids)
        ->update($params['fields']);
    }

    public function statusChange($id, Request $request){
        $request->validate([
            'status' => 'required',
            'user_id' => 'required',
            'role_id' => 'required'
        ]);

        $role = MemberRoles::find($id);
        return $role->where('user_id', $request->user_id)
        ->where('role_id',  $request->role_id)
        ->where('id', $id)
        ->update([
            'active' => $request->status == 1 ? '1' : '0'
        ]);
    }

    public function statusChangeAssociations($id, Request $request){
        $request->validate([
            'status' => 'required',
            'user_id' => 'required',
            'role_id' => 'required'
        ]);

        $role = MemberAssociations::find($id);
        if ($role->active == '1'){
            $role->active = '0';
        }else{
            $role->active = '1';
        }
        $role->save();
        return $role->active;
    }

    protected function deleteMemberAssociation($id){

    	$memberRole = MemberAssociations::findOrFail($id);

        $memberRole->delete();

        return new MemberAssocaitonResource($memberRole);
    }

    protected function deleteMemberRole($id){

    	$memberRole = MemberRoles::findOrFail($id);

        $memberRole->delete();

        return new MemberRolesResource($memberRole);
    }
}
