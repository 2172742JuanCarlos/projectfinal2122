<?php

namespace App\Http\Controllers;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Symfony\Component\Mime\Header\Headers;

define('BASE_URL', env('CIENCIA_VITAE_BASE_URL'));
define('CIIC_CLIENT_ID', env('CIENCIA_VITAE_API_USER'));
define('CIIC_CLIENT_SECRET', env('CIENCIA_VITAE_API_PASS'));
class GuzzleController extends Controller
{
    private function fetchUrl($path, $id, $opt_url_path, $headers = []) {
        try {
            $headers = $headers ?:  [ 'Accept' => 'application/json'];
            $client = new Client([
                'headers' => $headers
                ]);
            $data = $client->get(
                BASE_URL.$path.'/'.$id.'/'.$opt_url_path,
                [
                    'verify' => false, // this for cronjob
                    'auth' => [
                        CIIC_CLIENT_ID,
                        CIIC_CLIENT_SECRET
                    ],
                ]

            );
            return $data->getBody()->getContents();
        } catch (\Throwable $error) {
            return json_encode([
                'output' => '',
                'error' => $error
            ]);
        }

    }

    public function getRemoteCienciaVitaeDegrees($science_id = '')
    {
        $science = $science_id ?: auth('api')->user()->science_id;
        $data = $this->fetchUrl('curriculum', $science, 'degree');
        return $data;
    }

    public function getMyCurriculum($scienceid_from_users)
    {
        $headers = [
            'Content-Type' => ' application/pdf'
        ];
        $data = $this->fetchUrl('curriculum', $scienceid_from_users, '/pdf?attach-xml=false&lang=User%20defined', $headers);

        return $data;
    }

    public function getRemoteCienciaVitaePersonInfo()
    {
        $science = auth('api')->user()->science_id;
        $data = $this->fetchUrl('curriculum', $science, 'person-info');
        return $data;
    }

    public function getRemoteCienciaVitaeMailingAddresses()
    {
        $science = auth('api')->user()->science_id;
        $data = $this->fetchUrl('curriculum', $science, 'mailing-address');
        return $data;
    }

    public function getRemoteCienciaVitaeOutputs($science_id = '', $chache = false)
    {
        $cache = true; // remove if works properly
        $science = $science_id ?: auth('api')->user()->science_id;
        $data = $this->fetchUrl('curriculum', $science, 'output'. ($chache ? '/cached' : ''));
        //$data = collect($data)->values()->toArray();//->paginate(10);
        //$data = $this->paginate($data,10);
        //return response()->json($data,402);
        return $data;
    }

    public function paginate($items, $perPage, $page = null, $options = []) {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function getRemoteCienciaVitaePhones()
    {
        $science = auth('api')->user()->science_id;
        $data = $this->fetchUrl('curriculum', $science, 'phone-number');
        return $data;
    }

    public function getRemoteCienciaVitaeOutputs_By_Id($id = '')
    {
        if (empty($id)) {
            return '';
        }
        $data = $this->fetchUrl('curriculum', $id, 'output');
        return $data;
    }

    public function getRemoteCienciaVitaeWeb()
    {
        $science = auth('api')->user()->science_id;
        $data = $this->fetchUrl('curriculum', $science, 'web-address');
        return $data;
    }

    public function getRemoteCienciaVitaePrivileges()
    {
        $science = auth('api')->user()->science_id;
        $data = $this->fetchUrl('api-user', $science, 'access-privileges');
        return $data;
    }

    public function getRemoteCienciaVitaeAuthor()
    {
        $science = auth('api')->user()->science_id;
        $data = $this->fetchUrl('curriculum', $science, 'author-identifier');
        return $data;
    }

    public function getRemoteCienciaVitaeCitation()
    {
        $science = auth('api')->user()->science_id;
        $data = $this->fetchUrl('curriculum', $science, 'citation-name');
        return $data;
    }

    public function getRemoteCienciaVitaeLanguageCompetency()
    {
        $science = auth('api')->user()->science_id;
        $data = $this->fetchUrl('curriculum', $science, 'language-competency');
        return $data;
    }

    public function getRemoteCienciaVitaeEmployment()
    {
        $science = auth('api')->user()->science_id;
        $data = $this->fetchUrl('curriculum', $science, 'employment');
        return $data;
    }

    public function getRemoteCienciaVitaeGroups()
    {
        $science = auth('api')->user()->science_id;
        $data = $this->fetchUrl('curriculum', $science, 'groups');
        return $data;
    }

    public function getRemoteCienciaVitaeService($science_id = '')
    {
        $science = $science_id ?: auth('api')->user()->science_id;
        $data = $this->fetchUrl('curriculum', $science, 'service');
        return $data;
    }

    public function getRemoteCienciaVitaeFundings($science_id = '')
    {
        $science = $science_id ?: auth('api')->user()->science_id;
        $data = $this->fetchUrl('curriculum', $science, 'funding');
        return $data;
    }

    public function searchInstitutions($search, $only_names = true)
    {
        if (empty($search)) {
            return [];
        }
        $client = new Client([
            'headers' => [ 'Accept' => 'application/json']
            ]);
        $otherIdentifiers = \Request::get('otherIdentifiers') == 'true' ? 'true' : 'false';
        $order='Ascending';
        $pagination=\Request::get('pagination') == 'true' ? 'true' : 'false';
        $rows=10;
        $page=1;
        $lang='PT';
        $data = $client->get(
            BASE_URL.'searches/institutions?institutionName=' . $search
            .'&otherIdentifiers='. $otherIdentifiers
            .'&order=' . $order
            .'&pagination=' . $pagination
            .'&rows='. $rows
            .'&page='. $page
            .'&lang=' . $lang,
            [
                'verify' => false, // this for cronjob
                'auth' => [
                    CIIC_CLIENT_ID,
                    CIIC_CLIENT_SECRET
                ],
            ]
        );
        $data = $data->getBody()->getContents();

        $decoded_data = json_decode($data);
        $instituition_names = [];
        if (!empty($decoded_data) && is_array($decoded_data->result->institution)) {
            foreach ($decoded_data->result->institution as $ins) {
                $ins = (array) $ins;
                $instituition_names[] = $ins['institution-name'];
            }
        }
        return ($only_names) ? $instituition_names : $data;
    }
}
