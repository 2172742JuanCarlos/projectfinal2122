<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Exception;
use  Excel;

class RememberFillForm extends Controller
{
    public function readFile(Request $request)
    {

        $request->validate([
            'body' => 'required',
            'subject' => 'required',
            'file'  => 'required', //|mimes:xlsx, .csv
        ]);

        try {
            $content = $request->file('file');
            $file_type = $content->getClientOriginalExtension();
            $valid_file_types = ['xlsx','csv'];
            if (!in_array($file_type, $valid_file_types)) {
                return response()->json([
                    'status' => 'failed',
                    'message' => 'message',
                  ], 400);
            }
            $message_body = $request->body;
            $subject = $request->subject;

            $file_data = Excel::toArray([], $content->getPathname());
            $file_data = $file_data[0] ?? [];
            if (empty($file_data[0]) || (empty($message_body) || $message_body == 'undefined')) {
                return response()->json([
                    'status' => 'failed',
                    'message' => 'No file has been read!',
                ],200);
            }
            $macros = [];

            $email_key = 0;
            $macro_prefix = '#';
            if ($request->isDynamicMacros == 'false') {
                $id_key =  0;
                $name_key = 1;
                $email_key = 2;
                $user_name_key = 3;
                $password_key = 4;
                $status_key = 5;
                $is_file_filled_key = 6;
                $status_form_key = 7;
                $percentage_key = 8;

                $macros = [
                    $name_key => $macro_prefix . 'name',
                    $email_key => $macro_prefix . 'email',
                    $email_key => $macro_prefix . 'link' ,
                    $user_name_key => $macro_prefix . 'username' ,
                    $password_key => $macro_prefix . 'password',
                ];
            } else {
                foreach ($file_data[0] as $text) {
                    // remove spaces, special character, and lower case
                    $macro = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/','_', $text));
                    $macros[] = $macro_prefix . $macro;
                    if (str_contains('email',$macro)) {
                        $email_key = count($macros)-1;
                    }
                }
            }
            unset($file_data[0]);
                $result = [
                    'total_succeded' => 0,
                    'total_failed' => 0
                ];
            foreach ($file_data as $user_data) {
                $message_body = $request->body;
                foreach ($macros as $key => $macro) {
                    $message_body = str_replace($macro, $user_data[$key], $message_body);
                }

                if (empty($user_data) ||(empty($user_data[0]) && empty($user_data[1]) && empty($user_data[2]))) {
                    continue;
                }

                if (!empty($user_data[$email_key])) {
                    $to = $user_data[$email_key];

                    Mail::send([], [], function ($message) use($to, $subject, $message_body){
                        $message->to($to)
                        ->subject($subject)
                        ->setBody($message_body, 'text/html'); // for HTML rich messages
                    });
                    $result['total_succeded']++;
                } else {
                    $result['total_failed']++;
                    continue;
                }
            }

            return response()->json([
                'status' => 'success',
                'message' => $result,
              ], 200);

        } catch (Exception $error) {
            return response()->json([
                'status' => 'failed',
                'message' => 'message',
              ], 400);
        }
    }
}