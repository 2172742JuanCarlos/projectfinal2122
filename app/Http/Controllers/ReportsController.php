<?php

namespace App\Http\Controllers;

use App\Report;
use App\Http\Resources\ReportsResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\Element\TextRun;
use App\Http\Controllers\AwardController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\GuzzleController;
use App\Http\Controllers\ProjectsController;
use App\Http\Controllers\RoleController;
use Illuminate\Support\Facades\File;
use App\Enums\CVOutputType;
use App\Enums\CVActivities;
use App\Enums\CVDegrees;
use Exception;
use PhpOffice\PhpWord\Settings;

class ReportsController extends Controller
{
    protected $year;
    protected $users;
    protected $outputs;
    protected $numOuputs;

    private const PARAGRAPH = "<w:br/><w:br/>"; // Alternatives: '\n' or '<p>'

    public function getAll(){
        return ReportsResource::collection(Report::all());
    }

    /*
    * Get all Users
    *
    */
    protected function getUsers() : array
    {
        return (new UsersController)->getAll(true) ?: [];
    }

    /*
    * Set Users
    *
    */
    protected function setUsers($users) {
        $new_users = [];
        foreach($users as $key => $user) {
            $new_users[$user->id] = $user;
        }
        $this->users = $new_users;
    }

    /*
    * Set Year
    *
    */
    protected function setYear($year) {
        $this->year = $year;
    }

    /*
    * Set Output
    *
    */
    protected function setOutput($key, $value) {
        if (empty($key)) {
            return;
        }

        if (isset($this->outputs[$key])) {
            $this->outputs[$key] .= $value;
        } else {
            $this->outputs[$key] = $value;
        }

        self::setNumberOutputs($key);
    }

    protected function setNumberOutputs($key) {
        $old = 0;
        if (isset($this->numOuputs[$key])) {
            $old = (int) $this->numOuputs[$key];
        }
        $this->numOuputs[$key] = ++$old;
    }

    /*
    * Convert word paragraph
    *
    * @return Textrun, it requires to be set as setcomplexValue()
    * @see https://phpword.readthedocs.io/en/latest/templates-processing.html#setcomplexvalue
    */
    protected function convertParagraph($text = '',$break_line = true)
    {
        $textrun = new TextRun();
        if(!empty($text)) {
            $textlines = explode(self::PARAGRAPH, $text);
            $textrun->addText(array_shift($textlines));
            foreach ($textlines as $line) {
                if (!empty($line)) {
                    $textrun->addText($line);
                    if ($break_line) {
                        $textrun->addTextBreak();
                        $textrun->addTextBreak();
                    }
                }
            }
        }
        return  $textrun;
    }

    protected function getUserDegreeText($degree_code) {
        if(empty($degree_code)) {
            return '';
        }
        $degree = '';
        switch ($degree_code) {
            case CVDegrees::LICENCIATURE:
                $degree = 'BSc';
                break;
            case CVDegrees::MASTER:
                $degree = 'MSc';
                break;
            case CVDegrees::DOCTORATE:
                $degree = 'PhD';
                break;
        }
        return $degree;
    }

    /*
    *  Get parameters to generate file
    *
    */
    public function getParametersForFileReport(Request $request) {
        $request->validate([
            'from' => 'required',
            'to' => 'required',
        ]);

        $params = [];
        if (isset($request->format)) {
            $params['format'] = $request->format;
        }
        $from = $request->from;
        $to = $request->to;

        $date1 = date('Y-m-d H:i:s', (int) $from);
        $date2 = date('Y-m-d H:i:s', (int) $to);

        if($date1 > $date2) {
            return false;
        }

        return self::getFileReport(date('Y', $to), $from, $to, null, null, $params);
    }

    /*
    * Generate File Report
    *
    */
    public function getFileReport($year = 2000, $from_date = null, $to_date = null, $file_name = '', $save = false,$params = []){
        if (empty($from_date) && empty($to_date)){
            return false;
        }

        try {
            $content = [];
            $year = (int) $year;
            self::setYear($year);
            $path = 'file-templates/ReportTemplate.docx';
            if ($save) {
                $path = public_path()."/".$path;
            }
            $tpl_processor = new TemplateProcessor($path);
            /* Activate this if file contains to many special characters
            * Settings::setOutputEscapingEnabled(true);
            */

            $content['year'] = $year;
            $tpl_processor->setValue('year', $year);

            // Get all users --> later optimize to get only necessary users (issue may arrise with large table)
            $users = self::getUsers() ?? [];
            if(empty($users)) {
            return;
            }
            self::setUsers($users);

            self::getUsersOutputs($year, $tpl_processor, $from_date, $to_date);

            // Print all variable to the file
            foreach ($this->outputs as $key => $opt) {
                //$opt = htmlspecialchars($opt);
                $opt = preg_replace('/\s+/',' ',$opt);
                $textrun = self::convertParagraph($opt);
                $tpl_processor->setComplexValue($key, $textrun);
            }

            $number_of_outputs = '';
            foreach ($this->numOuputs as $key => $value) {
                $number_of_outputs .= $value .' '. ucfirst(str_replace(['_', '-'],' ',$key)) . ', ';
            }

            $textrun = self::convertParagraph($number_of_outputs);
            $tpl_processor->setComplexValue('number_of_outputs', $textrun ?? '');
            $ext = '.docx';
            $file_name = !empty($file_name)  ? $file_name.$ext : 'ActivitiesReport_' . $year . $ext;

            if ($save) {
                $folder = '/downloads';
                $path = $folder. "/".$file_name;
                $dir = public_path();
                $fullpath = $dir . $path;
                $tpl_processor->saveAs($fullpath);
                return [
                    'filename' => $file_name,
                    'path' => $path,
                    'status' => 'success'
                ];
            }
            $tpl_processor->saveAs($file_name);
            return response()->download($file_name)->deleteFileAfterSend(true);
        } catch (Exception $error) {
            echo "Error while getting file data. Please try again later or contact the administrator.";
            if ($save) {
                return [
                    'error' => $error,
                    'status' => 'failed'
                ];
            }
            return false;
        }
    }

    /*
    * Return awards
    *
    * Return array
    */
    protected function getAwards($user_id, $year) : array
    {
        $awards = (new AwardController)->getUserAwards($user_id);
        if(!$awards) {
            return [];
        }
        $to_print = '';
        $awards_users = [];
        foreach($awards as $award)
        {
            $awarded_at = !empty($award->awarded_at) ? : null;
            // TODO -Filter by date
            // Verify date
            $awards_users[] = $this->users[$award->user_id]->name ?? 'Sem nome';
            $to_print .= $award->title .' '.$this->year. ': '.str_replace(' ',' ',trim($award->description));
            $to_print .= self::PARAGRAPH;
        }

        $to_print = implode(', ', $awards_users) . self::PARAGRAPH . $to_print;

        return [
            'awards' => $awards,
            'text' => $to_print
        ];
    }

    /*
    * Return Ciencia Vitae Outputs
    *
    * @param int $cv_outuputs Publications,Teses, Articles (conference, magazine),Book, report
    * @param int $year Search by year
    * @return array
    * @see docs #/definitions/OutputTypeCtype
    */
    protected function getUsersOutputs($year = 0, &$tpl_processor, $from, $to) : array
    {
        if(!empty($output_type)) {
            return [];
        }
        $year = ($year === 0) ? date('Y') : $year;
        $user_outputs = [];
        $cv_api = new GuzzleController();

        foreach  ($this->users as $user) {
            $user_text = '';
            if (!empty($user->science_id)) {
                $user_id = $user->id;
                $user_science_id = $user->science_id;
                $user_info = [
                    'user_id' => $user_id,
                    'science_id' => $user_science_id
                ];
                // Get Awards
                $awards = self::getAwards($user_id, $year) ?? '';
                if(!empty($awards) && !empty($awards['text'])) {
                    $textrun = self::convertParagraph($awards['text']);
                    $tpl_processor->setComplexValue('awards', $textrun ?? '');
                }
                $degrees = self::getUserDegree($user_science_id, $year, $from, $to);
                $outputs = self::getUserOutputs($user_info, $cv_api, $year, $from, $to);
                $projects = self::getUserProjects($user_info, $cv_api, $year, $from, $to);
                $activities = self::getUserActivity($user_info, $cv_api, $from, $to);

                self::participationProAssoc($user_id, $user, $from, $to);

                $student = '';
                // Check if user was present in the report
                if($degrees['isUserPresent'] || $outputs || $projects || $activities) {
                    $degree_code = self::getUserDegreeText($degrees['degree_code']);
                    $user_text .= $user->name . ($degree_code ? ' ('. $degree_code .') ,' : '');
                    //$user_text .= self::PARAGRAPH;

                    $student .= in_array(strtolower($user->career),['student', 'Estudante']) ? $user_text : '';
                    $this->setOutput('students', $student);

                    $user_role = RoleController::getUserRole($user->id);
                    if (!empty($user_role)) {
                        // [ IMPORTANT ] Print File variable by Role name (eg. Full Member => full_member)
                        foreach($user_role as $val) {
                            $r = htmlspecialchars($val['name']);
                            $r = preg_replace('/\s+/','_', $r);
                            $r = strtolower($r);
                            if (str_contains($r,'external_council')) {
                                $user_text = $user->career . ' '.$user->name . ', '. $user->institution_name .', ';
                            }
                            $this->setOutput($r, $user_text);
                        }
                    }
                    if ($projects) {
                        $this->setOutput('researcher', $user_text);
                    }
                }
            }
        }
        return $user_outputs;
    }


    protected function getUserOutputs($user_info, $cv_api, $year, $from, $to)
    {
        $outputs = json_decode($cv_api->getRemoteCienciaVitaeOutputs($user_info['science_id'], true), true);

        $user_id = $user_info['user_id'] ?? 0;
        $user_science_id = $user_info['scrience_id'] ?? '';
        $user_outputs[$user_id] = $outputs;
        $user_outputs[$user_id]['user_id'] = $user_id;
        $user_outputs[$user_id]['scrience_id'] = $user_science_id;
        $books = '';
        $book_chapters = '';
        $journal_articles = '';
        $cientific_journal_articles = '';
        $pub_cientific_events = '';
        $participation_journal_articles = '';

        $result = $outputs['output'] ?? [];
        $totals = $outputs['totals'] ?? 0;
        if($totals <= 0 && !is_array($result)) {
            return;
        }
        $isUserPresent = false;
        foreach($result as $key => $output) {
            if(!empty($output['output-type']['code'])) {
                $isUserPresent = true;
                switch ($output['output-type']['code']) {
                    case CVOutputType::BOOK:
                        self::getBooksText($output['book'], $from, $to, $books);
                        if($output['book']['refereed']) {
                            $pub_cientific_events .= $books;
                        }
                        break;
                    case  CVOutputType::BOOK_CHAPTER:
                        self::getBooksChapterText($output['book-chapter'],$from, $to, $book_chapters);
                        if($output['book-chapter']['refereed']) {
                            $pub_cientific_events .= $books;
                        }
                        break;
                    case CVOutputType::JOURNAL_ARTICLE:
                        $journal_article = $output['journal-article'];
                        if(isset($journal_article['authoring-role']['value'])
                        && preg_match('/editor/',strtolower($journal_article['authoring-role']['value']))
                        ) {
                            self::getJournalText($journal_article, $year, $from, $to, $participation_journal_articles);
                        }
                        if ($journal_article['refereed']) {
                            self::getJournalText($journal_article, $year, $from, $to, $cientific_journal_articles);
                            $pub_cientific_events .= $cientific_journal_articles;
                        }
                        self::getJournalText($journal_article, $year, $from, $to, $journal_articles);
                        break;
                }
            };
        }
        $this->setOutput('books', $books);
        $this->setOutput('book_chapters', $book_chapters);
        $this->setOutput('cientific_journal_articles', $cientific_journal_articles);
        $this->setOutput('journal_articles', $journal_articles);
        $this->setOutput('participation_journal_articles', $participation_journal_articles);
        $this->setOutput('publications_cientific_events', $pub_cientific_events);
        return $isUserPresent;
    }

    protected function getUserDegree($user_science_id, $year, $from, $to)
    {
        $isUserPresent = false;
        $cv_api = new GuzzleController();
        $user_info = json_decode($cv_api->getRemoteCienciaVitaeDegrees($user_science_id), true);
        $degree = 0;
        if (isset($user_info['degree'])) {
            $ongoing_master_thesis = '';
            $accomplished_master_thesis = '';
            $ongoing_doctorate_thesis = '';
            $accomplished_doctorate_thesis = '';
            $isUserPresent = true;

            foreach ($user_info['degree'] as $key => $info) {
                $thesis = '';
                if ($info['degree-type']) {
                    $start_year = $info['end-date']['year'] ?? $year;
                    $end_year = $info['end-date']['year'] ?? [];

                    $degree_code = $info['degree-type']['code'];
                    if (isset($degree_code) && $degree_code > $degree) {
                        $degree = $info['degree-type']['code'];
                    }

                    $type = ($info['degree-type']['value'] === 'Mestrado') ? 'Mestrado' : 'Doutoramento';
                    $thesis .= $type. ' em '.  $info['degree-name'] ?? 'sem nome ' . ' ';
                    $thesis .= !empty($info['thesis']['thesis-title']) ? ' - "'.$info['thesis']['thesis-title'].'" ' : '';

                    if (isset($info['thesis']) && !empty($info['thesis']['supervisors']['supervisor'])) {
                        foreach ($info['thesis']['supervisors']['supervisor'] as $key => $values) {
                            $thesis .= isset($values['supervisor-name'] ) ? ' , '. $values['supervisor-name'] .' ' : '';
                        }
                    }
                    $thesis .= !empty($info['thesis']['institution']['institution-name']) ? ' - '.$info['thesis']['institution']['institution-name'].' ' : '';

                    if (!empty($end_year) && $end_year >= date('Y', $from) && $end_year <= date('Y', $to)) {
                        $thesis .=  ', '.($info['end-date']['day'] ?? '').' de '.(!empty($info['end-date']['month']) ? date('M', $info['end-date']['month']) :'').' de '.$end_year.' . ';
                    } elseif (!empty($info['start-date']['month']) && !empty($start_year)) {
                        $thesis .= 'no ano letivo '.$start_year.' , com entrega prevista '.date('M', ($info['end-date']['month']??$info['start-date']['month'])).' '.$end_year.';  ';
                    }
                    $thesis .= !empty($info['classification']) ? 'O Candidato foi aprovado com '. $info['classification'] .' valores;' : '';

                    $thesis .= self::PARAGRAPH;

                    // IF end this year, asssume thesis is done
                    if (!empty($end_year) && $end_year >= date('Y', $from) && $end_year <= date('Y', $to)) {
                        if ($info['degree-type']['value'] === 'Mestrado') {
                            $accomplished_master_thesis .=  $thesis;
                        } else {
                            $accomplished_doctorate_thesis .=  $thesis;
                        }
                    } else {
                        if (isset($info['degree-type']['value']) &&  $info['degree-type']['value'] === 'Mestrado') {
                            $ongoing_master_thesis .=  $thesis;
                        } else {
                            $ongoing_doctorate_thesis .=  $thesis;
                        }
                    }
                }
            }
            $this->setOutput('ongoing_master_thesis', $ongoing_master_thesis);
            $this->setOutput('accomplished_master_thesis', $accomplished_master_thesis);
            $this->setOutput('ongoing_doctorate_thesis', $ongoing_doctorate_thesis);
            $this->setOutput('accomplished_doctorate_thesis', $accomplished_doctorate_thesis);
        }
        return [
            'isUserPresent' => $isUserPresent,
            'degree_code' => $degree
        ];
    }

    protected function getJournalText($cv_content, $year = 0, $from, $to, &$content)
    {
        $publication_date = ($cv_content['publication-date']['year'] ?? $year) .'-'.($cv_content['publication-date']['month'] ?? '01' ).'-'.($cv_content['publication-date']['day'] ?? '1');
        $publication_date = strtotime($publication_date);
        if (self::validateDateInInterval($from, $to, $publication_date)) {
            $content .= !empty($cv_content['authors']['citation']) ? $cv_content['authors']['citation'] : '';
            $content .= ' ('. implode('/',array_values($cv_content['publication-date'])) .') ';
            $content .= !empty($cv_content['article-title']) ? $cv_content['article-title'] . ',' : '';
            $content .= !empty($cv_content['journal']) ? $cv_content['journal'] . ',' : '';
            $content .= !empty($cv_content['page-range-from']) ? ' (pp. '.$cv_content['page-range-from'].'-'.($cv_content['page-range-to'] ?? '').' )' : '';
            $content .= !empty($cv_content['volume']) ? ' Vol '. $cv_content['volume']. ' ' : '';
            $identifiers = !empty($cv_content['identifiers']['identifier']) ? array_splice($cv_content['identifiers']['identifier'], count($cv_content['identifiers']['identifier'])-1 ) : [];
            foreach ($identifiers as $identifier) {
                $content .= !empty($identifiers) ? strtoupper($identifier['identifier-type']['code']).': '. $identifier['identifier'] . '; ' : '';
            }
            $content .= !empty($cv_content['url']) ? ' ;' . $cv_content['url'].';' : '';
            $content .= self::PARAGRAPH;
        }
    }

    protected function getBooksChapterText($cv_content, $from, $to, &$content)
    {
        $from_year = (int) date('Y', $from);
        $to_year =  (int) date('Y', $to);
        $pub_year = (int) $cv_content['publication-year'];
        if (!empty($pub_year) && $pub_year >= $from_year && $pub_year <= $to_year ) {
            $content .= !empty($cv_content['authors']['citation']) ? $cv_content['authors']['citation'] : '';
            $content .= !empty($cv_content['publication-year']) ? ' ('. $cv_content['publication-year'] .') ' : '';
            $content .= !empty($cv_content['book-title']) ? $cv_content['book-title'] . ',' : '';
            $content .= !empty($cv_content['chapter-title']) ? $cv_content['chapter-title'] . ',' : '';
            $content .= !empty($cv_content['book-volume']) ? ' Vol '. $cv_content['book-volume']. ' '  : '';
            $content .= !empty($cv_content['book-edition']) ? ' ed '. $cv_content['book-edition'] . '.' : '';
            if (!empty($cv_content['chapter-page-range-from']) && !empty($cv_content['chapter-page-range-to'])) {
                $content .= ' (pp. '.$cv_content['chapter-page-range-from'].'-'.$cv_content['chapter-page-range-to'].') ';
            }
            $content .= empty($cv_content['book-publisher']) ? $cv_content['book-publisher'] .', ': '';
            $content .= !empty($cv_content['url']) ? ' DOI: ' . $cv_content['url'] : '';
            $content .= self::PARAGRAPH;
        }
    }

    protected function getBooksText($cv_content, $from, $to, &$content)
    {
        $from_year = (int) date('Y', $from);
        $to_year =  (int) date('Y', $to);
        $pub_year = (int) $cv_content['publication-year'];
        if (!empty($pub_year) && $pub_year >= $from_year && $pub_year <= $to_year ) {
            $content .= !empty($cv_content['authors']['citation']) ? $cv_content['authors']['citation'] .'; ' : '';
            $content .= !empty($cv_content['title']) ? $cv_content['title'] . '; ' : '';
            $content .= !empty($cv_content['publisher']) ? 'Editor: ' . $cv_content['publisher'] . '; ' : '';
            $content .= !empty($cv_content['volume']) ? ' Vol '. $cv_content['volume'] . '; ': '';
            $content .= !empty($cv_content['edition']) ? ' ed '. $cv_content['edition'] . '; ' : '';

            $book_identifiers = !empty($cv_content['identifiers']['identifier']) ? array_splice($cv_content['identifiers']['identifier'], count($cv_content['identifiers']['identifier'])-1 ) : [];
            foreach ($book_identifiers as $identifier) {
                $content .= !empty($book_identifiers) ? strtoupper($identifier['identifier-type']['code']).': '. $identifier['identifier'] . '; ' : '';
            }
            $content .= !empty($cv_content['publication-year']) ? ' ('. $cv_content['publication-year'] .') ' : '';
            $content .= self::PARAGRAPH;
        }
    }

    protected function getUserActivity($user_info, $cv_api, $from, $to)
    {
        $activities = json_decode($cv_api->getRemoteCienciaVitaeService($user_info['science_id'], true), true);
        $activities = $activities['service'] ?? [];
        $isUserPresent = false;
        if (empty($activities)) {
            return;
        }
        $isUserPresent = true;
        $graduate_examination = '';
        $graduate_doctorate_examination = '';
        $event_participation = '';
        $event_organization = '';

        foreach($activities as $key => $output) {
            if(!empty($output['service-category']['code'])) {
                switch ($output['service-category']['code']) {
                    case CVActivities::JURY_ACADEMIC_DEGREE:
                        $degree = $output['graduate-examination']['examination-degree']['value'] ?? '';
                        if($degree == 'Mestrado') {
                            self::getGraduateExaminationText($output['graduate-examination'], $from, $to, $graduate_examination);
                         } else if($degree == 'Doutoramento') {
                            self::getGraduateExaminationText($output['graduate-examination'], $from, $to, $graduate_doctorate_examination);
                        }
                        break;
                    case CVActivities::EVENT_ORGANISATION:
                        self::getEventText($output['event-administration'], $from, $to, $event_organization, $user_info);
                    break;
                    case CVActivities::EVENT_PARTICIPATION:
                        self::getEventText($output['event-participation'], $from, $to, $event_participation, $user_info);
                    break;
                };
            }
        }
        $this->setOutput('event_participation', $event_participation);
        $this->setOutput('event_organization', $event_organization);
        $this->setOutput('graduate_examination', $graduate_examination);
        $this->setOutput('graduate_doctorate_examination', $graduate_doctorate_examination);
        return $isUserPresent;
    }

    protected function getUserProjects($user_info, $cv_api, $year, $from, $to)
    {
        $fundings = json_decode($cv_api->getRemoteCienciaVitaeFundings($user_info['science_id'], true), true);
        $fundings = $fundings['funding'] ?? [];
        $isUserPresent = false;
        if (empty($fundings)) {
            return;
        }
        $isUserPresent = true;
        $projects = '';
        $research_projects = '';
        $project_ids = [];
        // TODO - Get all projects from BD and remove from array those that contain reference

        foreach($fundings as $opt) {
            $project = '';
            $start_date = [];
            $end_date = [];

            if (!empty($opt['start-date'])) {
                $start_date = $opt['start-date'];
                $end_date = $opt['end-date'] ?? [];
            } else {
                $start_date = $opt['start-date-participation'];
                $end_date = $opt['end-date-participation'] ?? [];
            }

            $start = ($start_date['year'] ?? $year) .'-'.($start_date['month'] ?? '01' ).'-'.($start_date['day'] ?? '1');
            $end = [];
            if (!empty($end_date)) {
                $end = ($end_date['year'] ?? $year) .'-'.($end_date['month'] ?? '01' ).'-'.($end_date['day'] ?? '1');
            }

            if (self::validateDateInInterval($from, $to, strtotime($start), strtotime($end))) {
                $project .= !empty($opt['status']['value']) ? '('.$opt['status']['value'].') ' : '';
                $project .= $opt['project-title'] .': ';
                $project .= !empty($opt['project-description']) ? $opt['project-description'].'; ' : '';
                if(!empty($opt['institutions']['institution'])){
                    $project .= 'parceiros desta iniciativa são ';
                    foreach ($opt['institutions']['institution'] as $institution) {
                        $project .= !empty($institution['institution-name']) ?  ', '.$institution['institution-name'].' ' :' Instituição anónima';
                    }
                 }
                if(!empty($opt['funding-institutions'])){
                    $project .= ' este projeto dispõe de financiamento das seguintes intituições ';
                    foreach ($opt['funding-institutions'] as $funding) {
                        $project .= !empty($funding['institution-name']) ?  ', '.$funding['institution-name'].' ' :' Instituição anónima';
                    }
                }
                $project .= !empty($opt['program-name']) ? 'Programa '. $opt['program-name'] .' em '. ($opt['year-awarded'] ?: '') .' ;' :'';

                $identifiers = $opt['funding-identifiers']['funding-identifier'];
                if(!empty($identifiers)){
                    $project .= ' identificadores: ';
                    foreach ($identifiers as $projet_ids) {
                        if($projet_ids['relationship-type']) {
                            unset($projet_ids['relationship-type']);
                        }
                        if($projet_ids['reference']) {
                            $project_ids[] = $projet_ids['reference'];
                        }
                        $project .= urldecode(http_build_query($projet_ids,'',', '));
                    }
                }
                $project .= self::PARAGRAPH;
            }

            if (!empty($opt['investigation-role'])
            && preg_match('#\b(investigador|researcher)\b#', strtolower($opt['investigation-role']['value']))
            ) {
               $research_projects .= $project;
            } else {
                $projects .= $project;
            }
        }

        // Get Projects From BD
        $DB_projects = ProjectsController::getProjectResearchersByUser($user_info['user_id'], $project_ids);

        if (!empty($DB_projects)) {
            foreach ($DB_projects as $p) {
                if ($p['id'] && $p['user_id'] && $p['created_at']) {
                    unset($p['id'],$p['user_id'],$p['created_at']);
                    if($p['updated_at']) {
                        unset($p['updated_at']);
                    }
                }
                $p = array_filter(array_values($p));
                $research_projects .= implode(', ',array_values($p));
                $research_projects .= self::PARAGRAPH;
            }
        }
        $this->setOutput('projects', $projects);
        $this->setOutput('research_projects', $research_projects);
        return $isUserPresent;
    }

    protected function getGraduateExaminationText($cv_content, $from, $to, &$content)
    {
        $year = $cv_content['date']['year'] ?? '';
        $date =  $year .'-'. ($cv_content['date']['month'] ?? '01' ).'-'.($cv_content['date']['day'] ?? '1');
        $date = !empty($date)  ? strtotime($date) : null;

        if (!empty($cv_content['date']) &&  self::validateDateInInterval($from, $to, $date)) {
            $content .= 'Júri de Provas Públicas do '.$cv_content['examination-degree']['value'] ?? 'Grau/Prova'.'';
            $content .= !empty($cv_content['examination-subject']) ? ' em '.$cv_content['examination-subject'].' '.$cv_content['course-code']['value'] ?? '.' :'';
            $content .= !empty($cv_content['student-name']) ? ' de '.$cv_content['student-name'].' ' :'';
            $content .= !empty($cv_content['theme']) ? 'com o tema “'.$cv_content['theme'].'” ' :'';

            if(!empty($cv_content['institutions']['institution'])){
                foreach ($cv_content['institutions']['institution'] as $key => $institution) {
                    $content .= !empty($cv_content['institution-name']) ?  ', '.$institution['institution-name'].' ' :' Instituição anónima';
                }
             }
            if (!empty($cv_content['date']['day']) && !empty($cv_content['date']['month'])) {
                $content .= ', '.$cv_content['date']['day'].' '.date('M', $cv_content['date']['month']).' de '.$year.' ;';
            } else {
                $content .= ', '. $year;
            }
            $content .= self::PARAGRAPH;
        }
    }

    protected function getEventText($cv_content,$from, $to, &$content, $user_info)
    {
        $user_name = $this->users[$user_info['user_id']]->name ?? 'Sem nome';
        $start_date = [];
        $end_date = [];

        if (!empty($cv_content['activity-start-date'])) {
            $start_date = $cv_content['activity-start-date'] ?? [];
            $end_date = $cv_content['activity-end-date'] ?? [];
        } elseif (!empty($cv_content['start-date'])) {
            $start_date = $cv_content['start-date'] ?? [];
            $end_date = $cv_content['end-date'] ?? [];
        } elseif (!empty($cv_content['event-start-date'])) {
            $start_date = $cv_content['event-start-date']  ?? [];
            $end_date = $cv_content['event-end-date']  ?? [];
        }

        $str_start_date = implode('-',$start_date);
        $str_end_date = implode('-',$end_date);

        if (isset($start_date) && self::validateDateInInterval($from, $to, strtotime($str_start_date), strtotime($str_end_date))) {
            $content .= isset($cv_content['event-type']['value']) ? $cv_content['event-type']['value'] . ', ' : '';
            $content .= $user_name . ', ';
            $content .= isset($cv_content['administrative-role']['value']) ? $cv_content['administrative-role']['value'] . ', ' : '';
            $content .= !empty($cv_content['event-description']) ? ' '.$cv_content['event-description'].', ' :'';
            $content .= !empty($cv_content['event-name']) ? ' '.$cv_content['event-name'].', ' :'';
            if(!empty($cv_content['institutions']['institution'])){
                foreach ($cv_content['institutions']['institution'] as $institution) {
                    $content .= !empty($cv_content['institution-name']) ?  ', '.$institution['institution-name'].' ' :' Instituição anónima';
                }
             }
            $content .= ' - '.($start_date['day'] ?? '').' '.($start_date['month'] ? date('M', $start_date['month']) : '').' de '.($start_date['year'] ?? '');

            if (!empty($end_date)) {
                $content .= ' - '.($end_date['day'] ?? '').' '.($end_date['month'] ? date('M', $end_date['month']) : '').' de '.($end_date['year'] ?? '');
            }
            $content .= ' ;';
            $content .= self::PARAGRAPH;
        }
    }

    public function participationProAssoc($user_id, $user_info, $from_date, $to_date)
    {
        $associations = (new AssociationController)->getByUserId($user_id) ?? [];
        $text = '';
        foreach($associations as $association) {
            if (!self::validateDateInInterval($from_date,$to_date,strtotime($association->created_at))) {
                continue;
            }
            $text .= $user_info->name .', '. $association->user_role .' do '.$association->association_name;
            $text .= !empty($association->abbreviation) ? '('.$association->abbreviation.')' :'';
            $text .= !empty($association->description) ? ', '. $association->description . '; ' : '; ';
            $text .= self::PARAGRAPH;
        }
        $this->setOutput('participation_pro_associations', $text);
    }

    public function addToQueue(Request $request) {
        $request->validate([
            'start_date' => 'required',
        ]);

        $format = $request->format ?? 'Y-m-d H:i:s';

        $end_date = !empty($request->end_date) ? $request->end_date : $request->start_date;

        $print_date =   Carbon::parse($request->start_date)->format('d_m_Y') .'_'.  Carbon::parse($request->end_date)->format('d_m_Y');

        $start_date = Carbon::parse($request->start_date)->format($format);
        $end_date = Carbon::parse($request->end_date)->format($format);

        $reports = new Report();
        $reports->name = 'ActivitiesReports_' . $print_date;
        $reports->path = '';
        $reports->status = 'P';
        $reports->start_date = $start_date;
        $reports->end_date = $end_date;
        $reports->created_at = Carbon::now();
        $reports->updated_at = Carbon::now();

        $reports->save();

        return new ReportsResource($reports);
        return;
    }

    /*
    * Check if given dates are between the dates intervals searched
    * Returns true if are between intervals, otherwise returns false.
    */
    public function validateDateInInterval($from, $to, $date_search_from, $date_search_to =null, $format = null)
    {
        $format = $format ?: 'Y-m-d';
        if(empty($from) && empty($to) && empty($date_search_from)) {
            return false;
        }

        $start_Date =  date($format, $from);
        $end_Date= date($format, $to);
        $date = date($format, $date_search_from);

        if (!empty($date_search_to)) {
            $date = date($format, $date_search_to);
        }

        return self::isDateBetweenDates($date, $start_Date, $end_Date);
    }

    function isDateBetweenDates($date, $startDate, $endDate) {
        return $date > $startDate && $date < $endDate;
    }
}
