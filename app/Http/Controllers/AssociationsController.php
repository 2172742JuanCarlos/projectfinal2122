<?php

namespace App\Http\Controllers;

use App\Http\Resources\AssociationResource;
use App\Association;
use Illuminate\Support\Facades\DB;

class AssociationsController extends Controller
{

    public function getAbout($id)
    {
        return Association::find($id);
    }

    public function getByUserId($user_id)
    {
        return DB::table('associations')
        ->select('associations.*', 'user_associations.user_role')
        ->join('user_associations', 'user_associations.id', '=','associations.id')
        ->where('user_associations.user_id','=', $user_id)
        ->get()->toArray();
    }
}
