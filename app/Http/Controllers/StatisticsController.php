<?php

namespace App\Http\Controllers;

use App\Http\Controllers\UsersController;
use App\User;
use App\Report;
use App\Project;
use App\Award;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

class StatisticsController extends Controller
{

    public function getTotalStatistics(Request $request)
    {
        $request->validate([
            'start_year' => 'required',
            'end_year' => 'required',
        ]);

        $start_year =  Carbon::parse((int) $request->start_year . '-01-01');
        $end_year =  Carbon::parse((int) $request->end_year . '-12-30');
        $outputs = [];

        $outputs['users'] = User::where('created_at', '<>', "Not defined.")
        ->whereBetween('created_at', [$start_year, $end_year])
        ->get()
        ->count();

        $outputs['reports'] = Report::where('created_at', '<>', "Not defined.")
        ->whereBetween('created_at', [$start_year, $end_year])
        ->get()
        ->count();

        $outputs['projects'] = Project::where('created_at', '<>', "Not defined.")
        ->whereBetween('created_at', [$start_year, $end_year])
        ->get()
        ->count();

        $outputs['awards'] = Award::where('created_at', '<>', "Not defined.")
        ->whereBetween('created_at', [$start_year, $end_year])
        ->get()
        ->count();

        return $outputs;
    }

    public function getTotalsCareerStatistics(Request $request)
    {
        $request->validate([
            'start_year' => 'required',
            'end_year' => 'required',
        ]);

        $start_year =  Carbon::parse((int) $request->start_year . '-01-01');
        $end_year =  Carbon::parse((int) $request->end_year . '-12-30');

        return  DB::table('users')
        ->select('career', DB::raw('count(*) AS total'))
        ->where('career','<>', '')
        ->whereBetween('created_at', [$start_year, $end_year])
        ->groupBy('career')
        ->get()->toArray();
    }

    public function getTotalsAdamecicDegreeStatistics(Request $request)
    {
        $request->validate([
            'start_year' => 'required',
            'end_year' => 'required',
        ]);

        $start_year =  Carbon::parse((int) $request->start_year . '-01-01');
        $end_year =  Carbon::parse((int) $request->end_year . '-12-30');
        return DB::table('users')
        ->select('academic_degree', DB::raw('count(*) AS total'))
        ->where('academic_degree','<>', '')
        ->groupBy('academic_degree')
        ->whereBetween('created_at', [$start_year, $end_year])
        ->get()->toArray();
    }

    function getTotalsActiveStatistics(Request $request)
    {
        $request->validate([
            'start_year' => 'required',
            'end_year' => 'required',
        ]);

        $data = [];
        $start_year =  Carbon::parse((int) $request->start_year . '-01-01');
        $end_year =  Carbon::parse((int) $request->end_year . '-12-30');

        $data['roles'] = DB::table('user_roles')
        ->selectRaw('year(start_date) as Year')
        ->selectRaw('SUM(active) as Total')
        ->where('active','=', '1')
        ->where(DB::raw('year(start_date)'),'<>','')
        ->whereBetween(DB::raw('year(start_date)'), [$start_year, $end_year])
        ->groupBy('start_date')
        ->orderBy('start_date')
        ->get()->toArray();

        $data['associations'] = DB::table('user_associations')
        ->selectRaw('year(created_at) as Year')
        ->selectRaw('SUM(active) as Total')
        ->where('active','=', '1')
        ->whereBetween('created_at', [$start_year, $end_year])
        ->groupBy(DB::raw('year(created_at)'))
        ->orderBy(DB::raw('year(created_at)'))
        ->get()->toArray();

        /*$data['users'] = DB::table('users')
        ->selectRaw('year(created_at) as Year')
        ->selectRaw('SUM(isActive) as Total')
        ->where('isActive','=', '1')
        ->whereBetween('created_at', [$start_year, $end_year])
        ->groupBy(DB::raw('year(created_at)'))
        ->orderBy(DB::raw('year(created_at)'))
        ->get()->toArray();*/

        return $data;
    }
}
