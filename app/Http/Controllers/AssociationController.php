<?php

namespace App\Http\Controllers;

use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use App\Association;
use App\Http\Resources\AssociationsResource;
use App\UserAssociations;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class AssociationController extends Controller
{
    public function getAll(){
        return AssociationsResource::collection(Association::all());
    }

    public function paginate($items, $perPage, $page = null, $options = []) {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function store(Request $request, $params = [])
    {
        $request->validate([
            'association_name' => 'required'
        ]);

        $association = new Association([
            'association_name' => $request->input('association_name'),
            'description' => $request->input('description') ?? '',
            'abbreviation' => $request->input('abbreviation') ?? ''
        ]);
        $association->save();
        if (!empty($params['return_data'])) {
            return $association;
        }
        return response()->json($association);
    }

    public function associateUserIntitution(Request $request){
        $request->validate([
            'association_name' => 'required',
            'associate_user' => 'required',
        ]);
        $association = Association::where('association_name', $request->association_name)->pluck('id');
        $association_id = $association->get('0');

        if (!$association_id) {
            $params['return_data'] = true;
            $result = self::store($request, $params);
            $association_id = $result->id;
        }
        if ($association_id && $request->associate_user) {
            return self::createAndAssociateUser($association_id);
        }
    }

    public function createAndAssociateUser($id, $user_id = ''){
        $user_id = empty($user_id) ? auth('api')->user()->id : $user_id;
        // No Relation Found
        $association = UserAssociations::where('associations_id', $id)
        ->where('user_id', $user_id)->get();
        if ($association->get(0)) {
            return response()->json($association->get(0));
        }

        $association = new UserAssociations([
            'user_id' =>  $user_id,
            'associations_id' => $id
        ]);
        $association->save();
        return response()->json($association);
    }


    public function show($id){
        $association = Association::find($id);
        return response()->json($association);
    }

    public function update($id, Request $request){
        $association = Association::find($id);
        $association->update($request->all());
        return response()->json('Association Updated !!');
    }

    public function destroy($id){
        $association = Association::find($id);
        $association->delete();
        return response()->json('Association Deleted');
    }

    public function disable($id){
        $association = UserAssociations::find($id);
        $user_id = auth('api')->user()->id;
        $association->where('user_id', $user_id)
        ->where('associations_id', $id)
        ->update([
            'active' => 0
        ]);
        return response()->json('Association Deleted');
    }

    public function getByUserId($user_id = '')
    {
        if (empty($user_id)) {
            $user_id = auth('api')->user()->id;
        }

        return DB::table('associations')
            ->select('associations.*', 'user_associations.user_role')
            ->join('user_associations', 'user_associations.id', '=','associations.id')
            ->where('user_associations.user_id','=', $user_id)
            ->where('active','=',1)
            ->get()->toArray();
    }

    public function getAllUserAssociations(){
        $associations = Association::join('user_associations','associations.id', '=', 'user_associations.associations_id')
            ->join('users', 'users.id', '=', 'user_associations.user_id')
            ->select('associations.association_name', 'users.name', 'user_associations.active', 'users.career')
            ->distinct()
            ->get()->toArray();


        return $associations ?? [1,2,3];
    }

    public function getAllAssociationsUsers(){

        $roles= DB::select('SELECT t.*, u.name , a.association_name , u.email, u.career FROM user_associations t, users u, associations a WHERE u.id = t.user_id AND a.id = t.associations_id AND t.created_at = (SELECT MAX(t2.created_at) FROM user_associations t2 WHERE t2.user_id = t.user_id) order BY u.name DESC');
        $roles = collect($roles)->values();
        $roles = $this->paginate($roles, 30);
        return $roles;
    }

    public function name(String $name){
        return AssociationsResource::collection(Association::where('association_name', '=', $name)->get());
    }

    public static function getUserAssociation($user_id, $params = '')
    {
        if(!$user_id) {
            return [];
        }

        $params['select'] = $params['select']  ?? 'associations.association_name';

        $role = Association::join('user_associations', 'user_associations.associations_id', '=', 'associations.id')
            ->join('users', 'users.id', '=', 'user_associations.user_id')
            ->where('users.id','=', $user_id)
            ->select($params['select'] )
            ->distinct()
            ->get()->toArray();

        return $role ?? [];
    }

//    public static function getUserAssociation($user_id)
//    {
//        if(!$user_id) {
//            return [];
//        }
//        $associations = Association::join('user_associations', 'user_associations.association_id', '=', 'associations.id')
//            ->join('users', 'users.id', '=', 'user_associations.user_id')
//            ->where('users.id','=', $user_id)
//            ->select('associations.association_name')
//            ->distinct()
//            ->get()->toArray();
//
//        return $associations ?? [];
//    }
}
