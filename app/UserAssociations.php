<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAssociations extends Model
{
    protected $fillable = [
        'user_id',
        'associations_id',
        'active',
        'user_role'
    ];
}
